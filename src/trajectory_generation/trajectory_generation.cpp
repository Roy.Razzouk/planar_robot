#include "trajectory_generation.h"
#include <iostream>

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO initialize the object polynomial coefficients
  pi=piIn;
  pf=pfIn; 
  Dt=DtIn;
  a = { piIn, 0, 0, 10, -15, 6 };
  
  
};

void          Polynomial::update(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO update polynomial coefficients
  Dt=DtIn;
  pi=piIn;
  pf=pfIn;
};


// POSITION ////////////---------------------------------------------------------------------------
const double  Polynomial::p     (const double &t){
  //TODO compute position
  double p = a[0]+ a[3]*(pf-pi)*pow(t/Dt,3) +a[4]*(pf-pi)*pow(t/Dt,4)+a[5]*(pf-pi)*pow(t/Dt,5); 
  return p;
};

// Velocity ////////////---------------------------------------------------------------------------
const double  Polynomial::dp    (const double &t){
  //TODO compute velocity
  double dp= 3*a[3]*(pf-pi)*pow(t,2)/pow(Dt,3) + 4*a[4]*(pf-pi)*pow(t,3)/pow(Dt,4)+ 5*a[5]*(pf-pi)*pow(t,4)/pow(Dt,5) ; 
  return dp;
};

 Point2Point::Point2Point(const Eigen::Vector2d & xi, const Eigen::Vector2d & xf, const double & DtIn){
  //TODO initialize object and polynomials
  polx= Polynomial(xi(0),xf(0),DtIn);
  poly= Polynomial(xi(1),xf(1),DtIn);
  Dt=DtIn;
 
}

Eigen::Vector2d Point2Point::X(const double & time){    //Vector X to be called
  //TODO compute cartesian position
  Eigen::Vector2d cartesien_position;
  cartesien_position= {polx.p(time),poly.p(time)};
  return cartesien_position;
}

Eigen::Vector2d Point2Point::dX(const double & time){
  //TODO compute cartesian velocity
  Eigen::Vector2d cartesien_velocity;
  cartesien_velocity= {polx.dp(time),poly.dp(time)};
  return cartesien_velocity;
}


