#include "trajectory_generation.h"
#include <iostream>

using namespace std;



const double Dtot=2;            // tf - ti
const double dt=0.000001; 

double step = 1e-3;             // 
Eigen::Vector2d  NumVel;

double t=0;
const Eigen::Vector2d Pi(0.2,0.4);
const Eigen::Vector2d Pf(1,0.9);
Point2Point trajectory = Point2Point (Pi, Pf, Dtot);

Eigen::Vector2d cart_pos;
Eigen::Vector2d cart_vel;


int main(){


// Display table titles 
cout  << "time"  << "\t Num_Vel (dx)"  << "\t cart_vel (dx)"  << "\t Num_Vel (dy)"  << "\t cart_vel (dy)"  << "\t cart_pos (x)" << "\t cart_pos (y)" << endl; 

  // Compute the trajectory for given initial and final positions. 
while (t<Dtot) {
cart_pos= trajectory.X(t);
cart_vel= trajectory.dX(t);

t=t+step;
// Display some of the computed points
// Check numerically the velocities given by the library 
// Check initial and final velocities

NumVel= (trajectory.X(t)-trajectory.X(t-dt))/dt;
cout  << t  << "\t\t" << NumVel(0) << "\t\t" << cart_vel (0) << "\t\t" << NumVel(1) << "\t\t" <<cart_vel (1) << "\t\t" << cart_pos(0) << "\t\t" << cart_pos(1) << endl; 

}

}