#include "control.h"
#include "trajectory_generation.h"
#include <iostream>


using namespace std;


//========================================
  const double l1In=0.4;
  const double l2In=0.5;
  const double DtIn= 5;
  const double dt=1e-1;
  double t=0;


  Eigen::Vector2d X_f (0.0 , 0.6);
  Eigen::Vector2d xd;
  Eigen::Vector2d Dqd;
  Eigen::Vector2d Dxd_ff;
  Eigen::Matrix2d J;
  Eigen::Vector2d X_i;
  Eigen::Vector2d Xa; 




int main(){
  // Show using all three libraries, simulate the motion of a planar robot
  // For a initial position 
  //myRobotModel.FwdKin( , , );
  // and  
  //      l1  = 0.4, l2 = 0.5
  // simulate a motion achieving 
  //      Xf  = 0.0, 0.6


  RobotModel myRobotModel(l1In,l2In);
  Controller myController(myRobotModel);

  Eigen::Vector2d q(M_PI_2, M_PI_4);
  myRobotModel.FwdKin(X_i,J,q);

  Point2Point myPoint2Point(X_i,X_f,DtIn);
  //cout " << "Xdesired \t\t "  << "Xactual" << "\n";

  while (t<DtIn){
    
    xd= myPoint2Point.X(t);
    Dxd_ff= myPoint2Point.dX(t);
    Dqd= myController.Dqd(q,xd,Dxd_ff);
    q= (Dqd*dt) + q;
    
    myRobotModel.FwdKin(Xa,J,q);
      cout <<  "\r" << "t :\n" << t << "\n" << "Xdesired : \n"  << xd << "\n" << "Xactual : \n" << Xa << "\n";

    t =t+dt;
  }
}

