#include "kinematic_model.h"
#include <iostream>

using namespace std;


Eigen::Matrix2d JOut(2,2);
Eigen::Vector2d xOut;
Eigen::Vector2d qIn(M_PI/3.0,M_PI/4.0);
const double l1In=3.0;
const double l2In=3.0;
RobotModel myRobotModel(l1In,l2In);

Eigen::Vector2d dq(M_PI/10,M_PI/10);
Eigen::Vector2d dx;
Eigen::Vector2d x;

int main(){
  // Compute the forward kinematics and jacobian matrix for 
  //      q =  M_PI/3.0,  M_PI_4 

  myRobotModel.FwdKin(xOut,JOut,qIn);
  cout <<" xOut = \n" << xOut << endl;
  cout <<"\n JOut = \n"<< JOut<< endl;

  // For a small variation of q, compute the variation on X and check dx = J . dq  

  qIn(0)=(M_PI/3.0)+dq(0);
  qIn(1)=(M_PI/4.0)+dq(1);

  //Position Variation dx with myRobotModel
  myRobotModel.FwdKin(x,JOut,qIn);
  cout<< "\n xOut-x =  \n"<< xOut-x <<"\n";

  //Position Variation dx with dx = J . dq 
  dx = JOut * dq;
  cout <<"\n dx = \n"<< dx << endl;
}


